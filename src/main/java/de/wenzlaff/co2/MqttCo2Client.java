package de.wenzlaff.co2;

import java.time.LocalDateTime;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Paho MQTT-CO2 Client.
 * 
 * Beispiele siehe
 * 
 * http://www.wenzlaff.info
 * 
 * und
 * 
 * https://jaxenter.de/iot-allrounder-27208
 * 
 * @author Thomas Wenzlaff
 * 
 */
public class MqttCo2Client {

	/** Der Server mit dem MQTT Broker. */
	private static final String DEFAULT_SERVER = "pi-bplus";

	/** Der abonnierte Topic. */
	private static final String TOPIC = "co2";

	/** ID Prefix für den Client. */
	private static final String CLIENT_ID = "id-";

	/**
	 * Startet eine MQTT Client der an den Topic co2 abonniert ist. Im Hintergurnd
	 * werden die Werte mit Bewertung auf System.out ausgegeben.
	 * 
	 * @param args der Server
	 * @throws MqttException bei verbindungs Problemen
	 * @throws Exception     bei Fehler
	 */
	public static void main(String[] args) throws Exception {

		String server = DEFAULT_SERVER;
		if (args.length == 1) {
			server = args[0];
		}

		MqttClient client = new MqttClient("tcp://" + server, generateClientId());

		System.out.println("Starte den CO2 MQTT-Client mit Server " + server + " und ID: " + client.getClientId() + " ...");

		client.setCallback(new MqttCallback() {

			@Override
			public void connectionLost(Throwable throwable) {
			}

			@Override
			public void messageArrived(String topic, MqttMessage nachricht) throws Exception {

				leseCo2(nachricht);
			}

			private void leseCo2(MqttMessage nachricht) {

				JSONObject jsonNachricht = new JSONObject(nachricht.toString());

				try {
					JSONArray nachrichten = jsonNachricht.getJSONObject("messung").getJSONArray("satz");
					JSONObject satz = (JSONObject) nachrichten.get(1);
					int co2 = satz.getInt("co2");

					System.out.println("CO2: " + co2 + " ppm");

					CO2Bewertung.bewertungUBAausgeben(co2);
					CO2Bewertung.bewertungDINEN1377Ausgeben(co2);

				} catch (JSONException e) {
					System.err.println(e);
				}
			}

			@Override
			public void deliveryComplete(IMqttDeliveryToken t) {
			}
		});

		client.connect();

		client.subscribe(TOPIC);
	}

	private static String generateClientId() {

		return CLIENT_ID + LocalDateTime.now();
	}
}
