package de.wenzlaff.co2;

/**
 * Co 2 Bewertungen.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class CO2Bewertung {

	private CO2Bewertung() {
		super();
	}

	/**
	 * DIN EN 13779 ldt. Absolute CO2-Konzentration in der Innenraumluft (ppm).
	 * 
	 * Quelle:
	 * https://www.hamburg.de/contentblob/1016516/7073fd694816ce7e8edd0eb82a2593e6/data/gesundheitliche-bewertung-co2.pdf
	 * 
	 * @param co2 Wert in ppm
	 */
	public static void bewertungDINEN1377Ausgeben(int co2) {
		if (co2 < 0) {
			throw new IllegalArgumentException("Die CO2 Werte können nicht negativ sein.");
		}
		if (co2 < 800) {
			System.out.println("Laut DIN EN 13779: Raumluft Kategorie: IDA 1 - Hohe Raumluftqualität");
		} else if (co2 > 800 && co2 < 1000) {
			System.out.println("Laut DIN EN 13779: Raumluft Kategorie: IDA 2 - Mittlere Raumluftqualität");
		} else if (co2 > 1000 && co2 < 1400) {
			System.out.println("Laut DIN EN 13779: Raumluft Kategorie: IDA 3 - Mäßige Raumluftqualität");
		} else {
			System.out.println("Laut DIN EN 13779: Raumluft Kategorie: IDA 4 - Niedrige Raumluftqualität");
		}
	}

	/**
	 * Quelle:
	 * https://www.hamburg.de/contentblob/1016516/7073fd694816ce7e8edd0eb82a2593e6/data/gesundheitliche-bewertung-co2.pdf
	 * 
	 * https://www.dguv.de/medien/ifa/de/pub/rep/pdf/reports2013/innenraumarbeitsplaetze/kapitel_12_4_1.pdf
	 * 
	 * @param co2 Wert in ppm
	 */
	public static LÜFTUNGSAMPEL bewertungUBAausgeben(int co2) {
		if (co2 < 0) {
			throw new IllegalArgumentException("Die CO2 Werte können nicht negativ sein.");
		}
		if (co2 < 1000) {
			System.out.println("Laut UBA GRÜN: Hygienisch unbedenklich, keine weiteren Maßnahmen");
			return LÜFTUNGSAMPEL.GRÜN;
		} else if (co2 >= 1000 && co2 < 2000) {
			System.out.println("Laut UBA GELB: Hygienisch auffällig, Lüftungsmaßnahmen erhöhen, Lüftungsverhalten überprüfen und verbessern");
			return LÜFTUNGSAMPEL.GELB;
		} else {
			System.out.println("Laut UBA ROT: Hygienisch inakzeptabel, Belüftbarkeit des Raumes prüfen ggf. weitergehende Maßnahmen prüfen");
			return LÜFTUNGSAMPEL.ROT;
		}
	}
}