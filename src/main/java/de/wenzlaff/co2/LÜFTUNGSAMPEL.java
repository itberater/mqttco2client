package de.wenzlaff.co2;

/**
 * Lüftungsampel.
 * 
 * @author Thomas Wenzlaff
 *
 */
public enum LÜFTUNGSAMPEL {
	/** Kohlendioxid-Leitwert als Lüftungsampel. */
	GRÜN, GELB, ROT
}
